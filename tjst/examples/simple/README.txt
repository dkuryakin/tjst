Install tjst with following command:
$ pip install tjst

Then compile templates with command:
$ tjst-compile tpl.html tpl.js

Then open index.html in your browser.