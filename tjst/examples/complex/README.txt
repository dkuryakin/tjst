Install tjst with following command:
$ pip install tjst

Then compile templates with command:
$ tjst-compile tpls tpls.js

Then open index.html in your browser.